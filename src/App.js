import React, {Component, lazy, Suspense } from 'react';
import './App.css';

import { BrowserRouter as Router,
  Routes,
  Route
} from 'react-router-dom';

const NavBar = lazy(() => import('./components/navbar'));
const Footer = lazy(() => import('./components/Footer'));
const About = lazy(() => import('./components/About'));
const Contacts = lazy(() => import('./components/Contacts'));
const Home = lazy(() => import('./components/Home'));

const renderLoader = () => <p>Loading</p>;


class App extends Component {
 render() {
  return (
    <Suspense fallback={renderLoader}>
    <Router basename={process.env.PUBLIC_URL}>
      <div className='container vh-95'>
        <NavBar />
        <div className='no-title bg-white text-center'>
          <h4>Fullstack Golang developer</h4>
          <hr></hr>
        </div>
        <Routes className='row'>
          <Route path="/*" element={<Home />} />
          <Route path="/home" element={<Home />} />
          <Route path="/about" element={<About />} />
          <Route path="/contacts" element={<Contacts />} />
        </Routes>
        <Footer  className='row bottom-0' />
      </div>
    </Router>
    </Suspense>
  );
}
}

export default App;
