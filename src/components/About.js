import React, { Component } from 'react'
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
// import AboutUs from '../FINAL LOGO.svg';
import ContactDetails from './ContactDetails';
 
export default class About extends Component {
  render() {
    return (
      <div className='text-justify'>
        <Card>
          <Card.Img variant="top" src='' />
          <Card.Body>
            <Card.Title>About</Card.Title>
            <Card.Text>
              
              <ContactDetails />
            </Card.Text>
          </Card.Body>
        </Card>
      </div>
    )
  }  
}