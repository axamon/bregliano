import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMailBulk } from '@fortawesome/free-solid-svg-icons'
import { faPhone } from '@fortawesome/free-solid-svg-icons'
import { faHome } from '@fortawesome/free-solid-svg-icons'


export default class ContactDetails extends Component {
  render() {
    return (
        <div>
            <ul class="list-unstyled mb-0">

                <li><i class="fas fa-envelope mt-4 fa-2x"></i>
                    <p><a href="https://it.linkedin.com/in/albertobregliano" target="_blank" rel="noopener noreferrer">Linkedin</a></p>
                    <p><FontAwesomeIcon icon={faMailBulk} /> <a href='mailto:alberto.bregliano@protonmail.com'>alberto.bregliano@protonmail.com</a></p>
                </li>
            </ul>
        </div>
    )
  }
}
