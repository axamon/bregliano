import React, { Component } from 'react'
import Card from 'react-bootstrap/Card'
import gopherastro from './gopherastro.png'
// import report from '../Flamingo PROFILE.pdf';
import ContactDetails from './ContactDetails';

export default class Contacts extends Component {
  render() {
    return (
      <div className='text-justify'>
        <Card>
            <Card.Img variant="top" src='' />
            <Card.Body>
              {/* <Card.Title>Contact Us</Card.Title> */}
              <Card.Text>
              <ContactDetails />
              <img src={gopherastro} />
              </Card.Text>
            </Card.Body>
        </Card>
      </div>
    )
  }  
}

