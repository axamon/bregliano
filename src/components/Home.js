import React, { Component } from 'react'
import Card from 'react-bootstrap/Card'
import gopherastro from './gopherastro.png'


export default class Home extends Component {
  render() {
    return (
      <div className='text-justify'>
        
        <Card>
            <Card.Img className="card" variant="top" src='' />
            <Card.Body>
              <Card.Title></Card.Title>
              <Card.Text>
              I develop in <a href='https://go.dev/'>Golang</a>, if you need help with that let me know.<br></br>
              <img src={gopherastro} />
              </Card.Text>
              {/* <a href='' attributes-list download><Button>Get more info</Button></a> */}
            </Card.Body>
        </Card>
      </div>
    )
  }  
}